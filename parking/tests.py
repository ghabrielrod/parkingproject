# Utils
import datetime

# Test dependencies
from django.test import TestCase
from rest_framework.test import APIRequestFactory

# Model dependencies
from .models import Reservation, Payment
from .forms import ReservationForm, PaymentForm

# View dependencies
from .views import ReservationListCreate, PaymentCreate


class ProjectTests(TestCase): 
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        cls.reservation = Reservation.objects.create(
            plate = "ASD-1234",
            paid = False,
            left = False,
            entry_date = "2021-04-04 00:00:00"
        ) 

    # (Example) Model tests --------------------------------------------------

    def test_falsy_plate_cases(self):
        possible_plates = ["12345678", "1235678", "", "1q2w3e4r", "aaaa-123", "aaa1234","asd1234","asd-1234"]

        for plate in possible_plates:
            form = ReservationForm(data={
                "plate": plate,
                "paid": False,
                "left": False,
                "entry_date": "2021-04-04 00:00:00",
            })
            self.assertFalse(form.is_valid())


    def test_validform_data(self): 
      form = ReservationForm(
        {
            "plate": "ASD-1234",
            "paid": False,
            "left": False,
            "entry_date": "2021-04-04 00:00:00",
        }
      )

      self.assertTrue(form.is_valid())

      reservation = form.save()

      self.assertEqual(reservation.plate, "ASD-1234")
      self.assertEqual(reservation.paid, False)
      self.assertEqual(reservation.left, False)

    def test_string_representation(self):        
        self.assertEqual(str(self.reservation), 
            "Reservation: %s - Plate: %s - Entry (%s)" % (
                self.reservation.id, 
                self.reservation.plate, 
                self.reservation.entry_date.strftime("%c"),)
        )

    # (Example) REST tests --------------------------------------------------
    
    def test_POST_Reservation(self):
        factory = APIRequestFactory()
        request = factory.post('/parking/', 
            {
            "plate": "AAA-1234",
            "entry_date": "2021-04-04 00:00:00",
            }        
        )
        view = ReservationListCreate.as_view()
        response = view(request)

        self.assertEqual(response.status_code, 201)

    # (Example) URLS tests --------------------------------------------------
    def test_GET_parking(self):
        response = self.client.get('/parking/')
        self.assertEqual(response.status_code, 200)
    
    
