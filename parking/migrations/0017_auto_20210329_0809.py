# Generated by Django 3.1 on 2021-03-29 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parking', '0016_auto_20210329_0807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='total_price',
            field=models.FloatField(max_length=10, null=True, verbose_name='Final price'),
        ),
    ]
