Parking
===

## The project 🧙
This is a project that was created to support a parking, with payments and some validation.

It was:
* Started on 27/03/2020
* Finished on 04/04/2020
* Period in hours: 12h

## Stack 💻
* Python
* Django
* Django Rest Framework

## Features ⭐️
* Home (/parking & /payment)
  * Fill the forms which validates the data in different ways. History of data (/history)

## How to install? 🔧
STEPS TO INSTALL THE APP in your local

- Clone the repo
- Run in cmd "source env/bin/activate" to enable the venv
- Run pip install -r requirements.txt to install dependencies in venv
- Create db config to your MySQL (DB_NAME, DB_USER, DB_PASS, PORT) or use the SQLLite config commented in setting.py to save time.
- Run in cmd "python manage.py migrate --run-syncdb" to update the db with some app dependencies

## Happy Path ⚙️
1. Access /parking/
2. Register a new vehicle (reservation)
3. Access /parking/(reservation_id_created)/pay and make the payments
4. Access /parking/(reservation_id_created)/out and make the checkout

The system was built to support different ways of validation and 'bad' use, like preventing wrong plates and duplicated reservations. Try to play around :)

## What I'd do differently 🔮💣️
1. I would move the vars to a .env

2. Having more time I probably would think how to increment the functionalities. 

3. I'd love to hear more about process regarding TDD with Django, or something like that. Sometimes it looks like Django does a lot of work alone, which is a good and bad thing, at the same time.

4. I had some issues with the 'final price'. I didn't know exactly how to use it on the model, calculating automatically (which I did) but removing it from the REST API. More to learn. 


## Final notes 📔
I am happy with the results.

___

