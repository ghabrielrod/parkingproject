# Utils
import uuid

# Model dependencies
from django.db import models
from .validators import validate_plate, validate_total_price

class Reservation(models.Model):
    serial_number = models.UUIDField("Serial number", primary_key=False, default=uuid.uuid4, editable=False, help_text="Reservation code")
    plate = models.CharField("Vehicle's plate", validators=[validate_plate], max_length=8)
    paid = models.BooleanField("Is it paid?", default=False, null=True, editable=False, blank=True, help_text="Did the customer pay the reservation?")
    left = models.BooleanField("Left?", default=False, null=True, editable=False, blank=True)
    entry_date = models.DateTimeField("Entry", auto_now_add=True)
    exit_date = models.DateTimeField("Exit", editable=False, null=True)
    
    def __str__(self):
        return "Reservation: %s - Plate: %s - Entry (%s)" % (self.id, self.plate, self.entry_date.strftime("%c"),)

class Payment(models.Model):
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE)
    cost_per_hour = models.PositiveIntegerField("Cost per hour", default=1, help_text="What's the cost per hour?")
    total_price = models.DecimalField("Final price", decimal_places=2, max_digits=100, null=True)
    created_at = models.DateTimeField("Created at", auto_now_add=True)    
