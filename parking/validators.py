from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils import timezone   
import re
from decimal import Decimal

def validate_plate(value):
    plate = value.replace('-','')

    # Brazilian plates should use the following resolution: 
    # https://www.in.gov.br/web/dou/-/resolucao-n-780-de-26-de-junho-de-2019-179414765
    # o.g "AAA1234"
    isValid = re.search("[A-Z]{3}[0-9][0-9A-Z][0-9]{2}", plate)

    if not isValid:
        raise ValidationError(
            _('%(value)s is not a valid plate number'),
            params={'value': value},
        )

def validate_total_price(reservation, cost_per_hour):
    entry_time = reservation.entry_date
    exit_time = timezone.now()
    cost_per_hour = cost_per_hour
    diff = exit_time - entry_time
    days, seconds = diff.days, diff.seconds
    hours = diff.total_seconds() / 60**2

    price = Decimal(hours) * Decimal(cost_per_hour)
    

    return round(price, 2)