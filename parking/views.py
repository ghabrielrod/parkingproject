# Utils 
from django.http.response import JsonResponse

# Rest imports
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

# Mpdel dependencies
from .models import Reservation, Payment
from .serializers import ReservationSerializer, PaymentSerializer
from .validators import validate_total_price

class HistoryView(generics.ListAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

    # List all reservations
    def get(self, request, *args, **kwargs):
        '''
        List all the reservations items
        '''
        reservations = Reservation.objects.filter(plate=kwargs['plate'])
        serializer = ReservationSerializer(reservations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class ReservationListCreate(generics.ListCreateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

    # List all reservations
    def get(self, request, *args, **kwargs):
        '''
        List all the reservations items
        '''
        reservations = Reservation.objects.all()
        serializer = ReservationSerializer(reservations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Create a new reservation
    def post(self, request, *args, **kwargs):
        '''
        Create the Reservation with given Reservation data
        '''
        paid = False
        left = False

        if not(request.data.get('paid')):
            paid = False
        else:
            paid = True

        if not(request.data.get('left')):
            left = False
        else:
            left = True

        reservations = Reservation.objects.all()

        # Checking if that plate is already present on the parking
        if reservations.filter(plate=request.data.get('plate')).filter(exit_date__isnull=True).exists():
            return Response(
                {"error": "That plate is already present in the parking and needs to finish the first process, paying and leaving before launching a new reservation."}, 
                status=status.HTTP_402_PAYMENT_REQUIRED
            )
    
        data = {
            "plate": request.data.get('plate'),
            "paid": paid,
            "left": left,
            "entry_date": request.data.get('entry_date'),
        }

        serializer = ReservationSerializer(data=data)

        if serializer.is_valid():
            serializer.save()

            #@DEV COMMENT about the test:
            # I decided to render the complete response for UX, but if needed the API could return only the JSON with the serial number
            #return JsonResponse({'reservation serial number': serializer.data.serial_number})
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReservationUpdate(generics.UpdateAPIView):
    queryset = Reservation.objects.all()
    lookup_field = 'id'
    serializer_class = ReservationSerializer

    def get(self, request, *args, **kwargs): 
        # Checking whether that reservation exists
        try:
            reservation = Reservation.objects.get(pk=kwargs['id'])
        except Exception:
            return Response(
                {"error": "That reservation doesn't exist."}, 
                status=status.HTTP_404_NOT_FOUND
            )

        serializer = ReservationSerializer(reservation, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        # Checking if that reservation exists
        try:
            reservation = Reservation.objects.get(pk=kwargs['id'])
        except Exception:
            return Response(
                {"error": "That reservation doesn't exist."}, 
                status=status.HTTP_404_NOT_FOUND
            )
            
        # Checking if there is a payment registered for that reservation id
        try:
            Payment.objects.get(reservation__pk=kwargs['id'])
        except Exception:
            return Response(
                {"error": "That reservation must be paid before the check out process."}, 
                status=status.HTTP_402_PAYMENT_REQUIRED
            ) 

        if reservation.exit_date != None:
            return Response(
                {"error": "The reservation for that plate is already paid and out of the parking."}, 
                status=status.HTTP_402_PAYMENT_REQUIRED
            )

        if request.data.get('plate') == '':
            return Response(
                {"error": "Please fill out the vehicle's plate before sending the checkout."}, 
                status=status.HTTP_400_BAD_REQUEST
            )

        # @DEV comment about the test: I realized we would be able to change the plate 
        #using PUT (checking out), a behavior that I wasn't sure if would be acceptable
        if not reservation.plate == request.data.get('plate'):
            return Response(
                {"error": "That reservation was not made for that vehicle's plate. The plate registered in a first moment was: %s" % (reservation.plate)}, 
                status=status.HTTP_404_NOT_FOUND
            )
    
        data = {
            "plate": request.data.get('plate'),
            "paid": True,
            "left": True,
        }

        serializer = ReservationSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PaymentCreate(generics.ListCreateAPIView):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    lookup_field = 'id'

    # List payments
    def get(self, request, *args, **kwargs):
        try:
            reservation = Reservation.objects.get(pk=kwargs['id'])

        except Exception:
            return Response(
                {"error": "That reservation doesn't exist."}, 
                status=status.HTTP_404_NOT_FOUND
            )

        payments = Payment.objects.filter(reservation__pk=reservation.pk)

        serializer = PaymentSerializer(payments, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Creating payment
    def post(self, request, *args, **kwargs):
        '''
        Create the Payment with given Payment data
        '''
        try:
            reservation = Reservation.objects.get(pk=kwargs['id'])
        except Exception:
            return Response(
                {"error": "That reservation doesn't exist and cannot be paid."}, 
                status=status.HTTP_404_NOT_FOUND
            )  

        if Payment.objects.filter(reservation__pk=reservation.pk).exists():
            return Response(
                {"error": "That reservation is already paid."}, 
                status=status.HTTP_400_BAD_REQUEST
            )

        total_price = validate_total_price(reservation, request.data.get('cost_per_hour'))

        data = {
            "reservation": request.data.get('reservation'),
            "cost_per_hour": request.data.get('cost_per_hour'),
            "total_price": total_price
        }

        serializer = PaymentSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)