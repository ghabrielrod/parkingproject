 
from django.forms import ModelForm

# Form dependencies
from .models import Reservation, Payment

class ReservationForm(ModelForm):
    class Meta:
        model = Reservation
        fields = '__all__'

class PaymentForm(ModelForm):
    class Meta:
        model = Payment
        fields = '__all__'

              